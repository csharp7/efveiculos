﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EFVeiculos.Models
{
    public partial class Veiculo
    {
        public string Placa { get; set; }
        public decimal? Ano { get; set; }
        public decimal? Km { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
    }
}
